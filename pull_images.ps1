
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

function create_list($date,$types)
{
    # This function will fetch the data at the URL for the provided date, and output a list of URLs to download
    #read-host $date
    foreach($item in $types){
        if($item -eq "natural"){
            $url_type = ""
        }else{
            $url_type = "$item"
        }
        $Url = "https://epic.gsfc.nasa.gov/$url_type`?date=$date"
        #read-host $url
        $iwr = Invoke-WebRequest -Uri $Url
        
        $images += $(($iwr).Images | Select-Object data-magnify-src | Where-Object -Property data-magnify-src -like "*.png" ) 
        
        # $images += $(($iwr).Images | Select-Object src) 
        #$images = $images | Where-Object -Property src -like "*/jpg/*"
    } 
    
    return $images
}

function fetch_files($date,$out_path)
{
    # This function will fetch files from the output of "create_list"

    $types=@("natural","enhanced")

    $list = create_list -date $date -types $types
    $progressPreference = 'silentlyContinue'    # Subsequent calls do not display UI.

    $active_jobs = @{}

    foreach($item in $list.'data-magnify-src')
    {

        #$date_url = $date.replace("-","/")
        $item_png_url = [string] $item
        $filename_png = $item.Split('/')[-1]

        #figure out if this is an enhanced or a natural image
        foreach($item in $types){
            if($item_png_url -like "*$item*"){
                $type = $item
            }
        }
        
        if(!(Test-Path "$out_path\$type")){
            New-Item -Path "$out_path\$type" -ItemType Directory -Force
        }
        $filepath = "$out_path\$type\$filename_png"

        #Read-Host($item_png)
        if (!(Test-Path $filepath))
        {
            Write-Host("Pulling $item_png_url...")
            $args = $item_png_url,$filepath

            $ScriptBlock = {
                $item_png_url = $args[0]
                $filepath = $args[1]
                if([Net.ServicePointManager]::SecurityProtocol -ne [Net.SecurityProtocolType]::Tls12){
                    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
                }
                Invoke-WebRequest -Uri $item_png_url -OutFile $filepath
            }
            
            $jobName = "$type`_$filename_png"
            
            if($active_jobs[$jobName].State -eq "Completed" ){
                Write-Host "$jobName already downloaded, skipping..."
            } elseif($active_jobs[$jobName].State -eq "Running" ){
                Write-Host "$jobName already running, skipping..."
            } else {
                $job = Start-Job -ArgumentList $args -ScriptBlock $ScriptBlock -Name $jobName
                $active_jobs.Add($jobName,$job) | Out-Null
            }
        } else {
            Write-Host "Found local copy of $filename_png! Skipping ^_^ "
        } 
    }
}

function pull_year($year)
{
    # This function loops through every DAY in the provided year and fetches ALL the files available from that date. 
    # Note, there are both natural and enhanced images to fetch at this time
    $current_date = Get-Date -UFormat "%Y-%m-%d"
    #read-host($current_date)
    #$i = 1
    
    if( ($(Get-Date).Year -eq $year) ) { $i = $(Get-Date).Month }
    else { $i = 12 }
    while($i -gt 0)
    {
        $i_mod = "{0:D2}" -f $i
        #create a new folder for that month
        $out_path = "\\localhost\earth\$year-$i_mod"
        if(!(test-path "$out_path"))
        {
            New-Item -ItemType Directory -Force -Path $out_path
        }
        
        $days = [DateTime]::DaysInMonth($year, $i)
        if(($(Get-Date).Month -eq $i) -and ($(Get-Date).Year -eq $year)) {$x = $(Get-Date).Day}
        else {$x = $days}
        
        while($x -gt 0)
        {
            $x_mod = "{0:D2}" -f $x
            $loop_date = "$year-$i_mod-$x_mod"
            Write-Host("$loop_date")
            fetch_files -date $loop_date -out_path $out_path
            $x--
            #verify we're not past the current date
            $loop_date_obj = (Get-Date $loop_date)
            $current_date_obj = (Get-Date $current_date)

            if($loop_date_obj -gt $current_date_obj){
                
            }
            start-sleep 2
        }
        $i--
    }

    Write-Host("We've gone too far Captain! We've gotta drop out!")
    exit

}
if($args[0]){
    pull_year($args[0])
}else{
    pull_year(Get-Date -UFormat "%Y")
}

