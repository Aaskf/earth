#This script will move pics of earth in to different categories. It will also make a .avi of time windows!

#this will rename any of the jpg files to png, I needed it when I screwed up downloading the files before. 
#Get-ChildItem -path *.jpg -Recurse | Rename-Item -NewName {$_.name -replace 'jpg','png' }

function sort_by_time()
{
    <#
    .SYNOPSIS
    This function will allow you to sort by time

    .DESCRIPTION
    This funciton will allow you to input a start and end time for when you want to fetch photos from, then copy those photos to their own folder.

    .PARAMETER start_time
    Use the format: HHmmss

    .PARAMETER end_time
    Use the format: HHmmss

    .PARAMETER copy
    Use this flag if you want to also copy the images off to a new folder

    .EXAMPLE
    sort_by_time()

    .NOTES
    General notes
    #>
    Param(
        [Parameter(Mandatory=$True)][string]$start_time,
        [Parameter(Mandatory=$True)][string]$end_time,
        [Parameter(Mandatory=$false)][string]$source_filepath,
        [Parameter(Mandatory=$false)][string]$destination_filepath,
        [Parameter(Mandatory=$false)][string]$working_filepath = "$PSScriptRoot",
        [Parameter(Mandatory=$false)][string]$type = "natural",
        [Parameter(Mandatory=$false)][switch]$copy = $false
        )    
    
    #Set your current running location
    #Set-Location -Path $working_filepath

    #Get all the pics from the raw_filepath folder, uncomment to run
    #$photo_list_obj = Get-ChildItem -path $source_filepath\*\$type\*.png -Recurse
    $i = 0
    #now we gotta loop through all the pics
    foreach($item in $photo_list_obj)
    {
        $item_date = $item.name.Substring(16,6)
        #Here is where we sort out the pics within our timeframe

        if($item_date -gt $start_time -And $item_date -lt $end_time)
        {

            #Here is where the copy flag says to copy if not exists
            if($copy -eq $True)
            {
                #Create filename
                #sequentially name the file
                $counter = ("{0:D4}" -f $i)
                #$filename = $item.name.Substring(0,23) + "_" + $counter + ".png"
                $filename = "image-" + $counter + ".png"

                $final_output_filepath = $destination_filepath + "\" + $filename
                
                #Write-Host("$final_output_filepath")
                If(!(Test-Path $final_output_filepath))
                {
                    Write-Host("Copying Item $item to $final_output_filepath")
                    if($env:OS){
                        Write-Host("Running on $($env:OS)!!!")
                        Copy-Item $item -Destination $final_output_filepath
                    }else{
                        #linux command here
                        #cp $item $final_output_filepath

                    }
                }else{
                    Write-Host("Found $final_output_filepath! Not copying!")
                }

                $i = $i + 1
            }else{
                Write-Host("Printing list: $item")
            }

        }
    }
    return $final_output_filepath
}
function create_avi($destination_filepath){
    #Set-Location -Path $destination_filepath
    $folder_list = Get-Item $destination_filepath/* | Get-Unique -AsString | Select-Object Name
    foreach($item in $folder_list.Name){
        #Read-Host($item)
        If(!(Test-Path $item\animation.avi))
        {
            Write-Host("Creating new AVI file")
            if(!$env:OS){
                avconv -f image2 -r 15 -i $destination_filepath/$item/image-%04d.png -q 4 $destination_filepath/$item/animation.avi
            }else{
                Write-Host "This is a $($env:OS) system! Use Linux at this step. "
            }
            
        }else{
            Write-Host("Found animation.avi for $destination_filepath_temp")
        }
    }   
}

$loop_start_time = [datetime]::ParseExact("000000","HHmmss",$null)
$loop_end_time = [datetime]::ParseExact("235900","HHmmss",$null)
$i = $loop_start_time

if($env:OS){
    Write-Host("Running on $($env:OS)!!!")
    $type = "natural"
    $source_filepath = "\\localhost\earth"
    $destination_filepath = "\\localhost\earth_output\$($args[0])"
    Write-Host("Pulling list of photos from the EARTH data store...Please wait...")
    $photo_list_obj = Get-ChildItem -path $source_filepath\*\$type\*.png -Recurse
    
}else{
    # Credentials for your local windows fileshare need to go here, it should be a local path on the linux VM
    $source_filepath = "/run/user/1000/gvfs/smb-share:domain=YOURSERVER,server=192.168.1.10,share=earth,user=password"
    $destination_filepath = "/run/user/1000/gvfs/smb-share:domain=YOURSERVER,server=192.168.1.10,share=earth_output,user=password"
    $type = "natural"
}


$resolution_in_minutes = 1439#15

$run = $true
while($run){
    #$input = Read-Host("Would you like to (C)ontinue or (L)ist out all files to be copied?")
    $input = "c"
    switch ($input.ToUpper()) {
        "C" { 
            Write-Host("Continuing!!!")
            $run = $false
         }
         "L"{
             Read-Host("You said you wanted it! The length of the photo_obj is $($photo_list_obj.Length)!!!")
             foreach($item in $photo_list_obj){Write-Host($item.Name)}
             Read-Host("Completed printout of $($photo_list_obj.Length) earth picture filenames")
             $run = $false
         }
        Default {
            Write-Host("Not a valid option! Try again!")
        }
    }
}

if($env:OS){
    while($i -lt $loop_end_time)
    {
        #Here is where we calculate the length in minutes that needs to pass
        $i_mod = "{0:HHmmss}" -f $i
        $x = $i.AddMinutes($resolution_in_minutes)
        $x_mod = "{0:HHmmss}" -f $x

        #If we are at 0 seconds, that means we are at the end of the day too
        if($x_mod -eq '000000')
        {
            $x_mod -eq '235900'
        }
        #read-host("Loop through imod: $i_mod to xmod: $x_mod")

        #Verify your output path exists
        $destination_filepath_temp = "$destination_filepath\$type\$i_mod`_$x_mod"

        if(!(Test-Path $destination_filepath_temp))
        {
            Write-Host("Couldn't find output directory. Creating new folder $destination_filepath")
            New-Item -ItemType Directory -Force -Path $destination_filepath_temp
        }


        #Here is where we call the fucntion that will actually sort our files in to the new location. 
        $output_path = sort_by_time -start_time "$i_mod" -end_time "$x_mod" -source_filepath $source_filepath -destination_filepath $destination_filepath_temp -copy

        Write-Host("Finished copying $output_path")

        if($i_mod -gt $loop_end_time)
        {
            $resolution_in_minutes_loop = $resolution_in_minutes - 1
        }else{
            $resolution_in_minutes_loop = $resolution_in_minutes
        }
        $i = $i.AddMinutes($resolution_in_minutes_loop)    
    }
}else{
    create_avi("$destination_filepath/$type")
}






