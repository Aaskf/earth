<#
.Synopsis
This script will copy the earth files from my hdd to the local system. Faster processing, woot!
#>
function copy_local(){
    $list = Get-ChildItem -Recurse d:\earth | Where-Object Name -like "*.png"

    if(!$(Test-Path c:\earth)){
        New-Item c:\earth -ItemType Directory
    }

    foreach($item in $list){
        $path = $item.FullName
        $split_path = $item.FullName.Split('\')
        
        $max_folders = $split_path.Count
        $i = 1
        while($i -le $max_folders){
            $temp_local_path = "$($split_path[$i])\$temp_local_path"
            $i++
        }
        Read-Host $temp_local_path
        $filename = $item.Name
        Write-Host "Checking to see if we already have $filename on the local host..."
        if(!$(Test-Path c:\earth\$temp_local_path\$filename)){
            Copy-Item -Path $path -Destination c:\earth\$temp_local_path -Verbose
            Write-Host "Successfully copied $filename to c:\earth ^_^"
        }else{
            Write-Host "Skipping $item, it already exists on the local host!!!"
        }
    }
}

switch($args[0]){
    '0' {
        Write-Host "Running in unattended mode!!!"
        copy_local
    }
    Default {
        Write-Host "No argument provided, exiting..."
    }    
}

