
BeforeAll {
  
    . $PSScriptRoot/init.ps1

}


Describe 'New-PSEarth' {

    
    It 'Given no parameters, it returns a tagline of "Hello World" ' {
        $(New-PSEarth).tagline | Should -Be "Hello World!"
    }
    It 'Given no parameters, it returns a url endpoint of the correct place' {
        $(New-PSEarth).urlRoot | Should -Be "https://epic.gsfc.nasa.gov"
    }

    #Context "Filtering by Name" {
#
    #  
#
    #    It "Given valid -Name '<Filter>', it returns '<Expected>'" -TestCases @(
#
    #        # We define an array of hashtables. Each hashtable will be used
    #        # for one test.
    #        # @{ Filter = 'ne*'  ; Expected = 'Neptune' }
    #        # Every hashtable has keys named as our parameters, that is Filter and Expected.
    #        # And values that will be injected in our test, in this case 'ne*' and 'Neptune'.
    #        @{ Filter = 'Earth'; Expected = 'Earth' }
    #        @{ Filter = 'ne*'  ; Expected = 'Neptune' }
    #        @{ Filter = 'ur*'  ; Expected = 'Uranus' }
    #        @{ Filter = 'm*'   ; Expected = 'Mercury', 'Mars' }
    #    ) {
#
    #        # We define parameters in param (), to pass our test data into the test body.
    #        # Parameter names must align with key names in the hashtables.
    #        param ($Filter, $Expected)
#
    #        # We pass $Filter to -Name, for example 'ne*' in our second test.
    #        $planets = Get-Planet -Name $Filter
    #        # We validate that the returned name is equal to $Expected.
    #        # That is Neptune, in our second test.
    #        $planets.Name | Should -Be $Expected
    #    }
#
    #    # Testing just the positive cases is usually not enough. Our tests
    #    # should also check that providing filter that matches no item returns
    #    # $null. We could merge this with the previous test but it is better to
    #    # split positive and negative cases, even if that means duplicated code.
    #    # Normally we would use TestCases here as well, but let's keep it simple
    #    # and show that Should -Be is pretty versatile in what it can assert.
    #    It "Given invalid parameter -Name 'Alpha Centauri', it returns `$null" {
    #        $planets = Get-Planet -Name 'Alpha Centauri'
    #        $planets | Should -Be $null
    #    }
    #}
}
