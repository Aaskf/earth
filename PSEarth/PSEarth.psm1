Class PSEarth {
    [String] $tagline 
    [String] $urlRoot

    PSEarth() {
        $this.tagline = "Hello World!"
        $this.urlRoot = "https://epic.gsfc.nasa.gov"
    }
    <# To Do List
        - [ ] create_list of available files
            - [ ] validate filenames and pull missing ones
            - [ ] validate file sizes and re-pull ones that are way too small
        - [ ] fetch_files
            - [ ] use the created lists to fetch files in parallel from the NASA servers
        
        - [ ] pull_year
    
    
    
    
    #>

}

function New-PSEarth(){

    $PSEarth = [PSEarth]::new();
    return $PSEarth

}